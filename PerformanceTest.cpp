#include "stdafx.h"
#include <thread>
#include <iostream>
#include <boost/test/unit_test.hpp>
#include "MultiQueueProcessor.h"
#include "StatAgent.h"
#include "Helpers.h"

BOOST_AUTO_TEST_SUITE(performans_tests)

static void PerformansTest(size_t aQueueCount, size_t aProducerCount, size_t aIterationCount)
{
	std::cout << "Queue: " << aQueueCount << "\t Producer: " << aProducerCount << "\t; Worker: " << aQueueCount << std::endl;
	std::cout << "Total Iterations: " << aIterationCount << std::endl;

	MultiQueueProcessor processor{ aQueueCount };
	ProducerKeeper producers(aProducerCount, processor.GetQueues(), aIterationCount);

	WorkerKeeper workers{ processor };

	StatAgentKeeper statAgents{ aIterationCount * aProducerCount };
	workers.SetStatAgents(statAgents);

	producers.Run();
	processor.Run();

	producers.Stop();

	processor.SetDone();
	processor.Stop();

	BOOST_TEST_CONTEXT("Check success processing")
	{
		BOOST_CHECK(workers.GetTotalSuccess() == aIterationCount * aProducerCount);
		BOOST_CHECK(producers.GetTotalSuccess() == aIterationCount / aQueueCount * aProducerCount);
	}

	statAgents.Print();
}

BOOST_AUTO_TEST_CASE(OneQueueOneProducerOneWorker)
{
	BOOST_TEST_CONTEXT("Small queue")
	{
		std::cout << "Small Queue" << std::endl;
		PerformansTest(1, 1, 60000);
	}

	BOOST_TEST_CONTEXT("Large queue")
	{
		std::cout << "Large Queue" << std::endl;
		PerformansTest(1, 1, 3000000);
	}
}

BOOST_AUTO_TEST_CASE(TwoQueueOneProducerTwoWorker)
{
	PerformansTest(2, 1, 3000000);
}

BOOST_AUTO_TEST_CASE(TwoQueueTwoProducerTwoWorker)
{
	PerformansTest(2, 2, 3000000);
}

BOOST_AUTO_TEST_CASE(OneQueueTwoProducerOneWorker)
{
	PerformansTest(1, 2, 300000);
}

BOOST_AUTO_TEST_CASE(ResubscribeWorkerWhileProcessing)
{
	using namespace std::chrono_literals;

	size_t iterationCount = 600000;

	std::cout << "Queue: " << 1 << "\t Producer: " << 1 << "\t; Worker: " << 1 << std::endl;
	std::cout << "Total Iterations: " << iterationCount << std::endl;

	MultiQueueProcessor processor{ 1 };

	Producer producer{ processor.GetQueues(), iterationCount };

	auto worker1 = std::make_shared<Worker<Queue>>();
	processor.Subscribe(worker1);

	StatAgent statAgent{ iterationCount };

	worker1->SetExternWork([&statAgent](const auto& aEvent)
	{
		statAgent.Process(aEvent);
	});

	auto worker2 = std::make_shared<Worker<Queue>>();
	worker2->SetExternWork([&statAgent](const auto& aEvent)
	{
		statAgent.Process(aEvent);
	});

	producer.Run();
	processor.Run();

	std::cout << "Threads started" << std::endl;
	std::cout << "Lock Worker" << std::endl;
	std::this_thread::sleep_for(1s);

	processor.Unsubscribe(worker1);

	std::cout << "Worker has processed until unsubscribe:" << worker1->GetSuccessed() << std::endl;

	std::cout << "Sleep for one second and unlock Worker" << std::endl;
	std::this_thread::sleep_for(1s);

	processor.Subscribe(worker2);

	producer.Stop();

	processor.SetDone();
	processor.Stop();

	std::cout << "Threads joined, check latency..." << std::endl;

	BOOST_TEST_CONTEXT("Check success processing")
	{
		BOOST_CHECK(worker1->GetSuccessed() + worker2->GetSuccessed() == iterationCount);
	}

	std::cout << "Estimated latency: " << statAgent.GetLatency() << "ns" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()