#pragma once
#include <numeric>
#include "Events.h"

template <class TEvent>
class StatContainer
{
public:
	void Process(const TEvent& aEvent)
	{
		mStatistic[mIndex++] = aEvent.GetEstimatedTime();
	}

	TElapsedTime GetLatency() const
	{
		if (mStatistic.empty())
		{
			return 0;
		}

		size_t count = 0;
		TElapsedTime total = 0;
		for (size_t i = 0; i < mStatistic.size(); ++i)
		{
			if (mStatistic[i] == 0)
			{
				continue;
			}

			total += mStatistic[i];
			++count;
		}

		if (count == 0)
		{
			return 0;
		}

		return static_cast<TElapsedTime>(
			static_cast<double>(total) / count);
	}

	void ReserveBuffer(size_t aSize)
	{
		mStatistic.clear();
		mStatistic.resize(aSize);
		mIndex = 0;
	}
private:
	std::vector<TElapsedTime> mStatistic;
	size_t mIndex;
};

class StatAgent
{
public:

	StatAgent(size_t aBufferSize)
	{
		mContainer.ReserveBuffer(aBufferSize);
	}

	void Process(const TimeEvent& aEvent)
	{
		mContainer.Process(aEvent);
	}

	TElapsedTime GetLatency() const
	{
		return mContainer.GetLatency();
	}

private:
	StatContainer<TimeEvent> mContainer;
};

