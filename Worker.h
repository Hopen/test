#pragma once

#include <iostream>
#include "Events.h"
#include "StatAgent.h"

template <class TQueue>
class Worker
{
public:
    using ExternWorkFunction = std::function<void(const TimeEvent& aEvent)>;

    Worker()
    {
        mIsSubscribed = false;
    }

	template <class TExternWork>
	void SetExternWork(TExternWork aExternWork)
	{
		mExternWork = aExternWork;
	}

	size_t GetFailed() const
	{
		return mFailedCounter;
	}

	size_t GetSuccessed() const
	{
		return mSuccessedCounter;
	}

    void MakeExternWork(const TimeEvent& aEvent)
    {
		if (!mExternWork)
		{
			return;
		}
        mExternWork(aEvent);
    }

    void ProcessResult(bool aSuccess)
    {
        aSuccess ? ++mSuccessedCounter : ++mFailedCounter;
    }

	void SetQueue(TQueue* aQueue)
	{
		mQueue = aQueue;
	}

	TQueue* GetQueue()
	{
		return mQueue;
	}

private:

    ExternWorkFunction mExternWork;
	TQueue* mQueue = nullptr;

	std::atomic_bool mIsSubscribed;

	size_t mFailedCounter = 0;
	size_t mSuccessedCounter = 0;
};

