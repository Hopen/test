#pragma once
#include <thread>
#include <iostream>

#include "MessageFactory.h"
#include "Queue.h"

class Producer
{
public:
	Producer(std::vector<Queue>& aQueues, size_t aIterations)
		: mQueues(aQueues)
		, mIterations(aIterations/ aQueues.size())
	{}

	void SendEvent()
	{
        using namespace std::chrono_literals;
		for (auto& queue : mQueues)
		{
			while (!queue.Push(MessageFactory::Generator::Make()))
			{
				++mFailedCounter;
			}
		}

		++mSuccessedCounter;
	}

	void Run()
	{
		mThread = std::make_unique<std::thread>([self = this]() 
		{
			for (size_t i = 0; i != self->mIterations; ++i)
			{
				self->SendEvent();
			}

			std::cout << "Producer Run finished, "
				<< self->GetSuccessed() << " messages has sent, "
				<< self->GetFailed() << " messages has failed"
				<< std::endl;
		});
	}

	void Stop()
	{
		assert("No thread" && mThread);
		if (mThread)
		{
			mThread->join();
		}
	}

	size_t GetFailed() const
	{
		return mFailedCounter;
	}

	size_t GetSuccessed() const
	{
		return mSuccessedCounter;
	}

private:
	std::vector<Queue>& mQueues;
	size_t mIterations;

	size_t mFailedCounter = 0;
	size_t mSuccessedCounter = 0;

	std::unique_ptr<std::thread> mThread;
};

