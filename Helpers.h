#pragma once

#include "Queue.h"
#include "Producer.h"
#include "Worker.h"
#include "StatAgent.h"

struct ProducerKeeper
{
	template <class... TArgs>
	ProducerKeeper(size_t aProducerCount, TArgs&&... aArgs)
	{
		for (size_t i = 0; i < aProducerCount; ++i)
		{
			Producers.emplace_back(Producer{ std::forward<TArgs>(aArgs)... });
		}
	}

	void Run()
	{
		for (auto& producer : Producers)
		{
			producer.Run();
		}
	}

	void Stop()
	{
		for (auto& producer : Producers)
		{
			producer.Stop();
		}
	}

	size_t GetTotalSuccess() const
	{
		size_t total = 0;
		for (auto& producer : Producers)
		{
			total += producer.GetSuccessed();
		}
		return total;
	}

	size_t GetTotalFailed() const
	{
		size_t total = 0;
		for (auto& producer : Producers)
		{
			total += producer.GetFailed();
		}
		return total;
	}

	std::vector<Producer> Producers;
};

struct WorkerKeeper
{
	using TWorker = Worker<Queue>;

	template <class TProcessor>
	WorkerKeeper(TProcessor& aProcessor)
	{
		for (size_t i = 0; i < aProcessor.GetQueueSize(); ++i)
		{
			auto worker = std::make_shared<TWorker>();
			aProcessor.Subscribe(worker);
			Workers.emplace_back(std::move(worker));
		}
	}

	template <class TStatAgentKeeper>
	void SetStatAgents(TStatAgentKeeper& aStatAgentKeeper)
	{
		for (auto& worker : Workers)
		{
			auto statAgent = aStatAgentKeeper.GetNextAgent();
			worker->SetExternWork([statAgent](const auto& aEvent)
			{
				statAgent->Process(aEvent);
			});
		}
	}

	size_t GetTotalSuccess() const
	{
		size_t total = 0;
		for (auto& worker : Workers)
		{
			total += worker->GetSuccessed();
		}
		return total;
	}

	size_t GetTotalFailed() const
	{
		size_t total = 0;
		for (auto& worker : Workers)
		{
			total += worker->GetFailed();
		}
		return total;
	}

	std::vector<std::shared_ptr<TWorker>> Workers;
};

struct StatAgentKeeper
{
	StatAgentKeeper(size_t aIterationCount)
		: IterationCount(aIterationCount)
	{
	}

	std::shared_ptr<StatAgent> GetNextAgent()
	{
		assert(IterationCount);

		auto statAgent = std::make_shared<StatAgent>(IterationCount);
		StatAgents.emplace_back(statAgent);
		return statAgent;
	}

	void Print() const
	{
		size_t count = 0;
		for (const auto& statAgent : StatAgents)
		{
			std::cout << "Estimated latency worker" << ++count << ": " 
				<< statAgent->GetLatency() << "ns" << std::endl;
		}
	}

	std::vector<std::shared_ptr<StatAgent>> StatAgents;
	size_t IterationCount = 0;
};