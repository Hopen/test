#pragma once
#include <vector>
#include <functional>
#include <thread>
#include "MessageFactory.h"
#include "Queue.h"


class MultiQueueProcessor
{
public:

	MultiQueueProcessor(size_t aQueueCount)
		: mQueues(aQueueCount)
	{
		mIsDone = false;
	}

    template <class TEvent>
    using TExternalWorkFunction = std::function<void(const TEvent&)>;

	void Subscribe(std::shared_ptr<Worker<Queue>>& aWorker)
	{
		auto& queue = GetFreeQueue();
		queue.SetWorker(aWorker);
		aWorker->SetQueue(&queue);
	}

	void Unsubscribe(std::shared_ptr<Worker<Queue>> aWorker)
	{
		auto* queue = aWorker->GetQueue();
		assert("Invalid queue" && queue);
		queue->SetWorker(nullptr);
		aWorker->SetQueue(nullptr);
	}

    void Run()
    {
        assert(mWorkerThreads.empty());

		for (auto& queue : mQueues)
		{
			std::thread queueThread([this, &queue]()
			{
				RunImpl(queue);
			});

			this->mWorkerThreads.emplace_back(std::move(queueThread));
		}
    }

    void Stop()
    {
        for (auto&& thread : mWorkerThreads)
        {
            thread.join();
        }
    }

	size_t GetQueueSize() const
	{
		return mQueues.size();
	}

	std::vector<Queue>& GetQueues()
	{
		return mQueues;
	}

	void SetDone()
	{
		mIsDone = true;
	}

private:

	Queue& GetFreeQueue()
	{
		auto it = std::find_if(mQueues.begin(), mQueues.end(), [](auto& aQueue)
		{
			return !aQueue.GetWorker();
		});

		if (it == mQueues.cend())
		{
			throw std::runtime_error("There is no more free queue");
		}

		return *it;
	}

    void RunImpl(Queue& aQueue)
    {
        auto worker = aQueue.GetWorker();
        if (!worker || IsDone())
        {
            // nothing to do
            return;
        }

		do
		{
			if (aQueue.IsNeedResubscribe())
			{
				worker = aQueue.GetWorker();
			}

			if (!worker)
			{
				continue;
			}

			WorkImpl(aQueue, worker);
		} 
		while (!IsDone());

        WorkImpl(aQueue, worker);
    }

    template <class TWorker>
    void WorkImpl(Queue& aQueue, std::shared_ptr<TWorker>& aWorker)
    {
        bool result;
		TimeEvent value;

        while (aQueue.IsSubscribed())
        {
            std::tie(value, result) = aQueue.Pop();
            if (!result)
            {
                aWorker->ProcessResult(result);
                break;
            }

            aWorker->ProcessResult(result);
            aWorker->MakeExternWork(value);
        }
    }

	bool IsDone() const
	{
		return mIsDone.load();
	}

private:
	std::atomic_bool mIsDone;

	std::vector<Queue> mQueues;
	std::vector<std::thread> mWorkerThreads;
};


