#pragma once
#include <mutex>
#include <boost/lockfree/queue.hpp>
#include "Events.h"
#include "Worker.h"

const size_t InitialQueueSize = 128;
const size_t MaxFixedQueueSize = 65534;

using TLockFreeQueue = boost::lockfree::queue<TimeEvent, boost::lockfree::fixed_sized<false>>;
using TLockFreeFixedQueue = boost::lockfree::queue<TimeEvent, boost::lockfree::fixed_sized<true>>;

template <class TQueueType, size_t QueueSize>
class QueueImpl
{
	using TWorker = Worker<QueueImpl<TQueueType, QueueSize>>;
public:
	QueueImpl() = default;
	QueueImpl(const QueueImpl&) = default;

	TQueueType Queue{ QueueSize };

	bool Push(TimeEvent aValue)
	{
		return Queue.push(aValue);
	}

	std::pair<TimeEvent, bool> Pop()
	{
		TimeEvent value;
		bool result = Queue.pop(value);
		return std::make_pair(value, result);
	}

    void SetWorker(const std::shared_ptr<TWorker>& aWorker)
    {
        std::lock_guard<std::mutex> lock(mMutex);
		mWorker = aWorker;
		mNeedResubscribe = true;
	}

	std::shared_ptr<TWorker> GetWorker()
    {
        std::lock_guard<std::mutex> lock(mMutex);
        mNeedResubscribe = false;
        return mWorker;
    }

	bool IsNeedResubscribe() const
	{
		return mNeedResubscribe;
	}

	bool IsSubscribed() const
	{
		return !mNeedResubscribe;
	}

private:

    std::shared_ptr<TWorker> mWorker;
	std::atomic_bool mNeedResubscribe {false};
    std::mutex mMutex;
};

using Queue = QueueImpl<TLockFreeFixedQueue, MaxFixedQueueSize>;
