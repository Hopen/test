#pragma once
#include <chrono>

using TElapsedTime = __int64;
//using TElapsedTime = __int64_t;

namespace MessageFactory
{
	struct TTimeEvent
	{
		friend struct Generator;
	public:

		TTimeEvent() = default;

		TElapsedTime GetEstimatedTime() const
		{
			return std::chrono::duration_cast<std::chrono::microseconds>
				(Initialize() - TimePoint).count();
		}

	private:

		using TTimePoint = std::chrono::time_point<std::chrono::system_clock>;

		static TTimePoint Initialize()
		{
			return std::chrono::system_clock::now();
		}

		TTimeEvent(TTimePoint aTimePoint)
			: TimePoint(std::move(aTimePoint))
		{
		}

	private:
		TTimePoint TimePoint;
	};
}

using TimeEvent = MessageFactory::TTimeEvent;
