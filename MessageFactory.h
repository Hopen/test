#pragma once

#include "Events.h"

namespace MessageFactory
{
	struct Generator
	{
		static TTimeEvent Make()
		{
			return TTimeEvent(TTimeEvent::Initialize());
		}
	};
}