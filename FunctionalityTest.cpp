#include "stdafx.h"
#include <thread>
#include <iostream>
#include <boost/test/unit_test.hpp>
#include "MultiQueueProcessor.h"
#include "Helpers.h"

BOOST_AUTO_TEST_SUITE(functionality_tests)

BOOST_AUTO_TEST_CASE(ShouldProcessOneEvent)
{
	auto process = [](size_t aQueueCount, size_t aProducerCount, size_t aIterationCount) 
	{
		MultiQueueProcessor processor{ aQueueCount };
		ProducerKeeper producers(aProducerCount, processor.GetQueues(), aIterationCount);
		
		WorkerKeeper workers{ processor };

		producers.Run();
		processor.Run();

		producers.Stop();

		processor.SetDone();
		processor.Stop();

		BOOST_TEST_CONTEXT("Check success processing")
		{
			BOOST_CHECK(workers.GetTotalSuccess() == aIterationCount);
			BOOST_CHECK(producers.GetTotalSuccess() == aIterationCount);
		}
	};

	process(1, 1, 1);
}

BOOST_AUTO_TEST_CASE(ShouldUnsubscribeAndSubscribeWorkerWhileProcessing)
{
	using namespace std::chrono_literals;

	MultiQueueProcessor processor { 1 };
	Producer producer{ processor.GetQueues(), 2 };

	auto worker = std::make_shared<Worker<Queue>>();
	processor.Subscribe(worker);

	auto lockWorker = [&worker, &processor]() 
	{
		processor.Unsubscribe(worker);
	};
	std::unique_ptr<std::thread> unlockWorkerThread;

	std::atomic_bool threadRun = false;
	auto unlockWorker = [&worker, &processor, &unlockWorkerThread, &threadRun]()
	{
		bool expected = false;
		if (!threadRun.compare_exchange_weak(expected, true))
		{
			return;
		}

		unlockWorkerThread = std::make_unique<std::thread>([&worker, &processor]()
		{
			std::cout << "Sleep for one second and unlock Worker" << std::endl;
			std::this_thread::sleep_for(1s);
			processor.Subscribe(worker);
			worker->SetExternWork([&processor](const auto&)
			{
				processor.SetDone();
			});
		});
	};

	worker->SetExternWork([&lockWorker, &unlockWorker](const auto& aEvent)
	{
		lockWorker();
		unlockWorker();
	});


	producer.Run();
	processor.Run();

	producer.Stop();
	processor.Stop();
		
	unlockWorkerThread->join();

	BOOST_TEST_CONTEXT("Check success processing")
	{
		BOOST_CHECK(worker->GetSuccessed() == 2);
		BOOST_CHECK(producer.GetSuccessed() == 2);
	}
}

BOOST_AUTO_TEST_CASE(ShouldUnsubscribeAndSubscribeNewWorkerWhileProcessing)
{
	using namespace std::chrono_literals;

	MultiQueueProcessor processor{ 1 };

	Producer producer{ processor.GetQueues(), 2 };

	auto worker1 = std::make_shared<Worker<Queue>>();
	processor.Subscribe(worker1);

	auto worker2 = std::make_shared<Worker<Queue>>();
	worker2->SetExternWork([](const auto& aEvent)
	{
		std::cout << "Worker2 make a job" << std::endl;
	});

	worker1->SetExternWork([&worker1, &worker2, &processor](const auto& aEvent)
	{
		processor.Unsubscribe(worker1);
		processor.Subscribe(worker2);
	});

	producer.Run();
	processor.Run();

	producer.Stop();
	processor.SetDone();
	processor.Stop();

	BOOST_TEST_CONTEXT("Check success processing")
	{
		BOOST_CHECK(worker1->GetSuccessed() == 1);
		BOOST_CHECK(worker2->GetSuccessed() == 1);
		BOOST_CHECK(producer.GetSuccessed() == 2);
	}
}

BOOST_AUTO_TEST_SUITE_END()